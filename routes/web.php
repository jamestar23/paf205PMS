<?php
use Carbon\Carbon;
use App\Personnel;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/', 'FrontController@index')->name('home'); // this is the native homepage

Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');



// Route::get('/home', 'HomeController@index');

// Route::group(['prefix' => 'admin', 'middleware' => ['auth','admin']], function () {  //this is the native admin route

// Admin
Route::group(['prefix' => '/', 'middleware' => ['auth','admin']], function () {
    Route::get('/', 'PersonnelController@dashboard', function () {

        return view('admin.index');

    })->name('admin.index');

    Route::resource('personnel','PersonnelController');
	Route::get('officers', 'PersonnelController@officers')->name('officers');
    Route::get('enlisted', 'PersonnelController@enlisted')->name('enlisted');
    
    Route::resource('user','UserController');
});


// User
// Route::group(['prefix' => '/', 'middleware' => ['auth']], 
// 	function () {
// 	Route::get('/home', 'HomeController@index', function () {
// 		return view('user.home');
// 	})->name('user.home');

// 	Route::resource('personnel','PersonnelController');
// 	Route::get('officers', 'PersonnelController@officers')->name('officers');
// 	Route::get('enlisted', 'PersonnelController@enlisted')->name('enlisted');
// });


