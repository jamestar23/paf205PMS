@extends('layout.main')

@section('content')
<div class="main-content">
    <header class="bg-primary text-white">
        <img src="{{asset('images/paf-banner.png')}}">
    </header>

    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <h2>205th Tactical Helicopter Wing</h2>
                    <p class="lead">
                        Tasked with conducting heli-lift operations in support to ground units of the Armed Forces of Philippines. It also supports nation-building activities of various Government Offices and Agencies, as well as Non-Government Organizations. The 205th Tactical Helicopter Wing has been at the cutting edge of Philippine Air Force operations, having been the pioneers of Night Vision technology in the PAF.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section id="mission" class="bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <h2>Mission</h2>
                    <p class="lead">To conduct tactical helicopter operations in support to the accomplishment of the PAF mission.</p>

                    <ul>
                        <li>
                            <i class="fas fa-chevron-circle-right"></i>
                            Conducts tactical airlift operations to include insertion and extrication of troops, deployment of combat forces, and logistics flights;
                        </li>

                        <li>
                            <i class="fas fa-chevron-circle-right"></i>
                            Conducts limited close air support and forward air control mission;
                        </li>

                        <li>
                            <i class="fas fa-chevron-circle-right"></i>
                            Conducts aero-medical evacuation;
                        </li>

                        <li>
                            <i class="fas fa-chevron-circle-right"></i>
                            Conducts air surveillance and reconnaissance;
                        </li>

                        <li>
                            <i class="fas fa-chevron-circle-right"></i>
                            Provides heli-lift requirement of law enforcement agencies and other government agencies;
                        </li>

                        <li>
                            <i class="fas fa-chevron-circle-right"></i>
                            Performs organizational and field maintenance of assigned aircraft and its associated equipment;
                        </li>

                        <li>
                            <i class="fas fa-chevron-circle-right"></i>
                            Conducts technical training to all aircrew, maintenance personnel and aerial door gunners, as well as OJT program on service support requirement of the units; and
                        </li>

                        <li>
                            <i class="fas fa-chevron-circle-right"></i>
                            Performs other functions as directed by higher Headquarters.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section id="history">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <h2>History</h2>
                    <p class="lead">Mactan Air Base began life as an emergency field for the American Strategic Air Command bombers to recover in the event of a war. This emergency field was basically barren with only a few permanent structures and a 10,000-foot concrete runway. By 1965, the only permanent structures on the field were a Philippine Air Force (PAF) operations building-cum-airline terminal and the PAF BOQ. There was also a squadron of PAF F-86s on base.</p>

                    <p class="lead">at least the 772nd and 774th Troop Carrier Squadrons of the 463rd Troop Carrier Wing (later renamed as "Tactical Airlift" Squadrons and Wing) as well as becoming a crew rest stop for C-124 and C-133 crews to relieve congestion at Clark Air Base. The C-124 Operations Squadron was the 606th Military Airlift Support Squadron or 606th MASS.</p>

                    <p class="lead">After the war, the base was transferred to the Philippine Air Force.</p>

                    <p class="lead">During the Mindanao campaign in the 1970s Mactan Air Base the base was extensively used for fighter operations against targets in Mindanao region by the Philippine Air Force.</p>

                    <p class="lead">In June 1996, Mactan Air Base was renamed Brig. Gen. Benito N. Ebuen Air Base in honor of a Philippine Air Force Commanding General.</p>

                    <p class="lead">In the aftermath of Super Typhoon Yolanda, Cebu was designated the hub of logistics for rescue and relief efforts and the base saw a continuous flow of helicopters and jumbo cargo aircraft. Each day, at least 10 were parked in the base including V-22 Ospreys of the US Marines to C-130 planes of different air forces of countries extending aid. Bigger aircraft like the C-5 Galaxy, C-17 Globemaster, 747 freighters and the Russian-made An-124 landed in the base.</p>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
