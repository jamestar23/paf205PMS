@extends('admin.layout.admin')

@section('content')

<h3 class="text-center content-header">List of Personnel</h3>
<hr>
<a href="#" class="btn btn-primary btn-add">
    <i class="fas fa-plus"></i> Add User
</a>
<!-- personnels called from personnel controller -->

<table id="personnel-list" style="width:100%" class="text-left table table-striped table-bordered">
  <tr>
    <th>ID</th>
    <th>Name</th>
    <th>Email</th>
    <th>Date Added</th>
  </tr>
  @forelse($users as $user) 
  <tr>
    <td>{{$user->id}}</td>
    <td>{{$user->name}}</td>
    <td>{{$user->email}}</td>
    <td>{{$user->created_at}}</td>
  </tr>
  @empty
    <h3>This is impossible!</h3>
  @endforelse
</table>




<!-- ------------------------------- -->

@endsection