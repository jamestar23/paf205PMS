<!DOCTYPE html>
<html>
<head>
    <title>Admin Area</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" media="screen" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/admin.css')}}">
    <link rel="stylesheet" href="{{asset('css/styles.css')}}">
</head>
<body>
<div class="wrapper">
    @include('admin.layout.includes.header')
    <div class="page-content">
        @if(Session::has('message'))
            <div class="alert alert-info">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif

        <div class="container">
            <div class="row">
                <div class="col-md-3" style="margin-top:20px;">
                    @include('admin.layout.includes.sidenav')
                </div>
                <div class="col-md-9 display-area" style="margin-top:20px;">
                    <div class="content-box-large">
                        @yield('content')
                    </div>

                </div><!--/Display area after sidenav-->
            </div>
        </div>

    </div><!--/Page Content-->
    <div class="push"></div>
</div>
<!-- Footer -->
<footer class="py-5 bg-dark">
  <div class="container">
    <p class="m-0 text-center text-white">Copyright &copy; Philippine Air Force 205th Tactical Helicopter Wing 2018</p>
  </div>
  <!-- /.container -->
</footer>

<!-- <script src="https://code.jquery.com/jquery.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        $(".submenu > a").click(function (e) {
            e.preventDefault();
            var $li = $(this).parent("li");
            var $ul = $(this).next("ul");

            if ($li.hasClass("open")) {
                $ul.slideUp(350);
                $li.removeClass("open");
            } else {
                $(".nav > li > ul").slideUp(350);
                $(".nav > li").removeClass("open");
                $ul.slideDown(350);
                $li.addClass("open");
            }
        });
    });
</script>


<script>
    jQuery(document).ready(function () {
        jQuery('.rank-cat > select#rank').change(function(){
        var rankData = $(this).find(':selected').text();
        var catData = $('input#category').val();

        if (rankData=='Airman'||rankData=='Airman Second Class'||rankData=='Airman First Class'||rankData=='Sergeant'||rankData=='Staff Sergeant'||rankData=='Technical Sergeant'||rankData=='Master Sergeant'||rankData=='Senior Master Sergeant'||rankData=='Chief Master Sergeant')
        {
            jQuery('input#afpsn').val('');
            jQuery('input#afpsn').attr('maxLength', '6') ;
            jQuery('input#category').val('Enlisted Personnel');
            catData = 'Enlisted Personnel';
            $value = catData;
        } else {

            jQuery('input#afpsn').val('O-');
            jQuery('input#afpsn').attr('maxLength', '7') ;
            jQuery('input#category').val('Officer');
            catData = 'Officer';
            $value = catData;
        }
        });
    });

    jQuery(document).ready(function () {
        jQuery('.stat > select#status').change(function(){
        var statData = $(this).find(':selected').text();
        var sDate = $('input#statusDate');
        var sDateL = $('label[for="statusDate"]');
        var gDays = $('input#daysGiven');
        var gDaysL = $('label[for="daysGiven"]');
            
        if (statData != "Duty" && statData !="Others")
        {
            $(sDate).attr('style','display:block !important;');
            $(sDateL).attr('style','display:block !important;');
            $(gDays).attr('style','display:block !important;');
            $(gDaysL).attr('style','display:block !important;');
        } else {
            $(sDate).attr('style','display:none');
            $(sDateL).attr('style','display:none');
            $(gDays).attr('style','display:none');
            $(gDaysL).attr('style','display:none');
        }
        
        });
        
        
    });


$(document).ready(function() {
    $("input#afpsn").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});

// $(document).ready(function() {
//     var stat = $('td.statDatefrom').text();
//     if (stat ="leave"){
//         $('th.statDateto').text(stat + " started on");
//     }
//     else if (stat ="hospitalized"){
//         $('th.statDateto').text(stat + " on");
//     }

    
// });
</script>
</body>
</html>
