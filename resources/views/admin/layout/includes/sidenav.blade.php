{{-- Side Navigation --}}

<div class="sidebar content-box" style="display: block;">
    <ul class="nav">
        <!-- Main menu -->
        <li class="current"><a href="{{route('admin.index')}}"><i class="glyphicon glyphicon-home"></i>
                Dashboard</a></li>
        <li class="submenu">
            <a href="#">
                <i class="fas fa-fighter-jet"></i> Personnel
                <span class="caret pull-right"></span>
            </a>
            <!-- Sub menu -->
            <ul>
                <li>
                    <a href="{{route('personnel.index')}}">View All</a>
                </li>
                <li>
                    <a href="{{route('officers')}}">Officers</a>
                </li>

                <li>
                    <a href="{{route('enlisted')}}">Enlisted Personnels</a>
                </li>

                <li>
                    <a href="#">Civilian Employees</a>
                </li>
            </ul>
        </li>
        @if(Auth::user()->isAdmin())
        <li class="submenu">
            <a href="#">
                <i class="fas fa-user"></i> Users
                <span class="caret pull-right"></span>
            </a>
            <!-- Sub menu -->
            <ul>
                <li>
                    <a href="{{route('user.index')}}">All Users</a>
                </li>
                <li>
                    <a href="#">Add Users</a>
                </li>
            </ul>
        </li>
        @else
            
        @endif
    </ul>
</div>
 <!-- ADMIN SIDE NAV-->