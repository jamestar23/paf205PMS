@extends('admin.layout.admin')
@section('content')
	
    <h3 class="content-header">Dashboard</h3>
    <hr>

    <h3>Recently added Personnels</h3>
    <p>Date Today: {{$dtoday}}</p>
	<table id="personnel-list" style="width:100%" class="text-left table table-striped table-bordered">
	  <tr>
	    <!-- <th>ID</th> -->
	    <th>Rank</th>
	    <th>Last Name</th> 
	    <th>First Name</th>
	    <th>Middle Name</th>
	    <th>AFP Serial No.</th> 
	    <th>Assigned</th> 
	    <th>Status</th>
	    <th>Status Started at</th>
	    <th>Remarks</th>
	    <!-- <th>Image</th> -->
	  </tr>
	  @forelse($newPersonnel as $newP) 
	  <tr>
	    <!-- <td>{{$newP->id}}</td>	 -->
	    <td>{{$newP->rank}}</td>
	    <td>{{$newP->lname}}</td> 
	    <td>{{$newP->fname}}</td>
	    <td>{{$newP->mname}}</td>
	    <td>{{$newP->afpsn}}</td> 
	    <td>{{$newP->assigned}}</td>
	    <td>{{$newP->status}}</td>
	    <td>{{$newP->statusDate}}</td>
	    <td>{{$newP->remarks}}</td> 
	  </tr>
	  @empty
	    <h3>No Personnel</h3>
	  @endforelse
	</table>
@endsection