@extends('admin.layout.admin')

@section('content')

<h3 class="text-center content-header">List of Personnel</h3>
<hr>
<a href="{{route('personnel.create')}}" class="btn btn-primary btn-add">
    <i class="fas fa-plus"></i> Add Personnel
</a>
<a href="#" class="btn btn-primary btn-add">
    <i class="fas fa-plus"></i> Add Civilian Employee
</a>
<!-- personnels called from personnel controller -->

<table id="personnel-list" style="width:100%" class="text-left table table-striped table-bordered">
  <tr>
    <!-- <th>ID</th> -->
    <th>Rank</th>
    <!-- <th>Last Name</th> 
    <th>First Name</th>
    <th>Middle Name</th> -->
    <th>Name</th>
    <th>AFP Serial No.</th> 
    <th>Assigned</th> 
    <th>Status</th>
    <th>Status started at</th>
    <th>Remarks</th>
    <th>Action</th>
    <!-- <th>Image</th> -->
  </tr>
  @forelse($personnels as $personnel) 
  <tr>
    <!-- <td>{{$personnel->id}}</td> -->
    <td>{{$personnel->rank}}</td>
    <!-- <td>{{$personnel->lname}}</td> 
    <td>{{$personnel->fname}}</td>
    <td>{{$personnel->mname}}</td> -->
    <td>{{$personnel->lname}}, {{$personnel->fname}} {{$personnel->mname}}</td>
    <td>{{$personnel->afpsn}}</td> 
    <td>{{$personnel->assigned}}</td>
    <td>{{$personnel->status}}</td>
    <td>{{$personnel->statusDate}}</td>
    <td>{{$personnel->remarks}}</td> 
    <!-- <td><img src="{{url('images',$personnel->image)}}" alt="2x2" width="96" height="96" /></td>  -->
    <td>
        <a href="{{route('personnel.edit',$personnel->id)}}" >Edit</a>
        |
        <a href="{{action('PersonnelController@show', $personnel['id'])}}">View</a>
    </td> 
  </tr>
  @empty
    <h3>No Personnel</h3>
  @endforelse
</table>




<!-- ------------------------------- -->

@endsection