@extends('admin.layout.admin')

@section('content')
<h3 class="text-center content-header">List of Officers</h3>
<hr>
<!-- personnels called from personnel controller -->

<table id="personnel-list" style="width:100%" class="text-left table table-striped table-bordered">
  <tr>
    <!-- <th>ID</th> -->
    <th>Rank</th>
    <!-- <th>Last Name</th> 
    <th>First Name</th>
    <th>Middle Name</th> -->
    <th>Name</th> 
    <th>AFP Serial No.</th> 
    <th>Assigned</th> 
    <th>Status</th>
    <th>Status started at</th>
    <th>Remarks</th>
    <th>Action</th>
    <!-- <th>Image</th> -->
  </tr>
  @forelse($officers as $officer) 
  <tr>
    <!-- <td>{{$officer->id}}</td> -->
    <td>{{$officer->rank}}</td>
    <!-- <td>{{$officer->lname}}</td> 
    <td>{{$officer->fname}}</td>
    <td>{{$officer->mname}}</td> -->
    <td>{{$officer->lname}}, {{$officer->fname}} {{$officer->mname}}</td>
    <td>{{$officer->afpsn}}</td> 
    <td>{{$officer->assigned}}</td>
    <td>{{$officer->status}}</td>
    <td>{{$officer->statusDate}}</td>
    <td>{{$officer->remarks}}</td> 
    <!-- <td><img src="{{url('images',$officer->image)}}" alt="2x2" width="96" height="96" /></td>  -->
    <td>
        <a href="{{route('personnel.edit',$officer->id)}}" >Edit</a>
        |
        <a href="{{action('PersonnelController@show', $officer['id'])}}">View</a>
    </td> 
  </tr>
  @empty
    <h3>No Personnel</h3>
  @endforelse
</table>

<hr>

<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home">Recap</a></li>
  <li><a data-toggle="tab" href="#duty">On Duty</a></li>
  <li><a data-toggle="tab" href="#leave">On Leave</a></li>
  <li><a data-toggle="tab" href="#hospitalized">Hospitalized</a></li>
  <li><a data-toggle="tab" href="#others">Others</a></li>
</ul>

<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    <table id="personnel-list" style="width:100%" class="text-left table table-striped table-bordered">
      <tr>
        <th><b>On Duty</b></th>
        <th><b>On Leave</b></th>
        <th><b>Hospitalized</b></th>
        <th><b>Others</b></th>
      </tr>
      <tr>
        <td>{{count($officerOD)}}</td>
        <td>{{count($officerOL)}}</td>
        <td>{{count($officerOH)}}</td>
        <td>{{count($officerO)}}</td>
      </tr>
    </table>
  </div>
  <div id="duty" class="tab-pane fade">
    <table id="personnel-list" style="width:100%" class="text-left table table-striped table-bordered">
      <tr>
        <th>Rank</th>
        <th>Last Name</th> 
        <th>First Name</th>
        <th>Middle Name</th>
        <th>AFP Serial No.</th> 
        <th>Assigned</th> 
        <th>Remarks</th>
        <!-- <th>Image</th> -->
      </tr>
      @forelse($officerOD as $officer) 
      <tr>
        <td>{{$officer->rank}}</td>
        <td>{{$officer->lname}}</td> 
        <td>{{$officer->fname}}</td>
        <td>{{$officer->mname}}</td>
        <td>{{$officer->afpsn}}</td> 
        <td>{{$officer->assigned}}</td>
        <td>{{$officer->remarks}}</td> 
      </tr>
      @empty
        <h3>No Personnel On Duty</h3>
      @endforelse
    </table>
  </div>
  <div id="leave" class="tab-pane fade">
    <table id="personnel-list" style="width:100%" class="text-left table table-striped table-bordered">
      <tr>
        <th>Rank</th>
        <th>Last Name</th> 
        <th>First Name</th>
        <th>Middle Name</th>
        <th>AFP Serial No.</th> 
        <th>Assigned</th> 
        <th>Remarks</th>
        <!-- <th>Image</th> -->
      </tr>
      @forelse($officerOL as $officer) 
      <tr>
        <td>{{$officer->rank}}</td>
        <td>{{$officer->lname}}</td> 
        <td>{{$officer->fname}}</td>
        <td>{{$officer->mname}}</td>
        <td>{{$officer->afpsn}}</td> 
        <td>{{$officer->assigned}}</td>
        <td>{{$officer->remarks}}</td> 
      </tr>
      @empty
        <h3>No Personnel On Leave</h3>
      @endforelse
    </table>
  </div>
  <div id="hospitalized" class="tab-pane fade">
    <table id="personnel-list" style="width:100%" class="text-left table table-striped table-bordered">
      <tr>
        <th>Rank</th>
        <th>Last Name</th> 
        <th>First Name</th>
        <th>Middle Name</th>
        <th>AFP Serial No.</th> 
        <th>Assigned</th> 
        <th>Remarks</th>
        <!-- <th>Image</th> -->
      </tr>
      @forelse($officerOH as $officer) 
      <tr>
        <td>{{$officer->rank}}</td>
        <td>{{$officer->lname}}</td> 
        <td>{{$officer->fname}}</td>
        <td>{{$officer->mname}}</td>
        <td>{{$officer->afpsn}}</td> 
        <td>{{$officer->assigned}}</td>
        <td>{{$officer->remarks}}</td> 
      </tr>
      @empty
        <h3>No Personnel Hospitalized</h3>
      @endforelse
    </table>
  </div>
  <div id="others" class="tab-pane fade">
    <table id="personnel-list" style="width:100%" class="text-left table table-striped table-bordered">
      <tr>
        <th>Rank</th>
        <th>Last Name</th> 
        <th>First Name</th>
        <th>Middle Name</th>
        <th>AFP Serial No.</th> 
        <th>Assigned</th> 
        <th>Remarks</th>
        <!-- <th>Image</th> -->
      </tr>
      @forelse($officerO as $officer) 
      <tr>
        <td>{{$officer->rank}}</td>
        <td>{{$officer->lname}}</td> 
        <td>{{$officer->fname}}</td>
        <td>{{$officer->mname}}</td>
        <td>{{$officer->afpsn}}</td> 
        <td>{{$officer->assigned}}</td>
        <td>{{$officer->remarks}}</td> 
      </tr>
      @empty
        <h3>No Personnel</h3>
      @endforelse
    </table>
  </div>
</div>
@endsection