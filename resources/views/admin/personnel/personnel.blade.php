@extends('admin.layout.admin')

@section('content')




<div id="personnel-indvl" class="row">
  <div class=" col-md-6 col-md-offset-3 ">
    <div class="thumbnail text-center">
    <p><b>AFP Serial No: {{$personnel->afpsn}}</b></p>
    <img src="{{url('images',$personnel->image)}}" alt="2x2" width="200" height="100" />
    <hr>
    <p><strong>{{$personnel->lname}}, {{$personnel->fname}} {{$personnel->mname}}</strong></p>
    <hr>
    <p>{{$personnel->rank}}</p>
    <hr>
    <p><b>Designation</b> <br> {{$personnel->assigned}}</p>
    <p><b>Status</b> <br> {{$personnel->status}}</p>
    <p><b>{{$personnel->status}} Started at</b> <br> {{$personnel->statusDate}}</p>
    <p><b>Remarks</b> <br> {{$personnel->remarks}}</p>
    <hr>
    
    <div class="action">
        <a href="{{route('personnel.edit',$personnel->id)}}" class="edit-btn">Edit</a>
        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#del-btn">
          Delete
        </button>

    </div>

    <!-- spacer -->
    <br>

    <strong>{{$eDate}}</strong>
  </div>   
  </div>
</div>




<!-- Modal -->
<div class="modal fade" id="del-btn" tabindex="-1" role="dialog" aria-labelledby="hayme" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h4>Are you sure you want to remove this Personnel?</h4>
      </div>
      <div class="modal-footer">
        <form action="{{route('personnel.destroy',$personnel->id)}}"  method="POST">
            {{csrf_field()}}
            {{method_field('DELETE')}}
            <input class="btn btn-danger" type="submit" value="Delete">
        </form>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
@endsection



