
@extends('admin.layout.admin')

@section('content')

	<h3 class="content-header">Edit Personnel</h3>
	<hr>
		<div class="full-width">
            {!! Form::model($personnel,['route' => ['personnel.update',$personnel->id], 'method' => 'PUT', 'files' => true]) !!}
			<!-- names -->
            <div class="row">
                <div class="form-group col-md-4 ">
                    {{ Form::label('lname', 'Last Name') }}
                    {{ Form::text('lname', null, array('class' => 'form-control','required'=>'','minlength'=>'2')) }}
                </div>
                <div class="form-group col-md-4">
                    {{ Form::label('fname', 'First Name') }}
                    {{ Form::text('fname', null, array('class' => 'form-control','required'=>'','minlength'=>'3')) }}
                </div>

                <div class="form-group col-md-4">
                    {{ Form::label('mname', 'Middle Name') }}
                    {{ Form::text('mname', null, array('class' => 'form-control','required'=>'','minlength'=>'5')) }}
                </div>
            </div>

            <!-- AFP Info -->

            <div class="row">
                <div class="form-group col-md-4 rank-cat">
                    {{ Form::label('rank', 'Rank') }}
                    {{ Form::select('rank',
                    [ 
                        'Airman' => 'Airman',
                        'Airman Second Class' => 'Airman Second Class',
                        'Airman First Class' => 'Airman First Class',
                        'Sergeant' => 'Sergeant',
                        'Staff Sergeant' => 'Staff Sergeant',
                        'Technical Sergeant' => 'Technical Sergeant',
                        'Master Sergeant' => 'Master Sergeant',
                        'Senior Master Sergeant' => 'Senior Master Sergeant',
                        'Chief Master Sergeant' => 'Chief Master Sergeant',

                        'Secont Lieutenant' => 'Secont Lieutenant',
                        'First Lieutenant' => 'First Lieutenant',
                        'Captain' => 'Captain',
                        'Major' => 'Major',
                        'Lieutenant Colonel' => 'Lieutenant Colonel',
                        'Colonel' => 'Colonel',
                        'Brigadier General' => 'Brigadier General',
                        'Major General' => 'Major General',
                        'Lieutenant General' => 'Lieutenant General',
                        'General' => 'General'
                    ], 
                       null,
                      ['class' => 'form-control','placeholder'=>'Select Rank']) }}
                </div>
                <div class="form-group col-md-4">
                    {{ Form::label('afpsn', 'AFP Serial Number') }}
                    {{ Form::text('afpsn', null, array('class' => 'form-control','type'=>'number','maxlength'=>'7')) }}
                </div>
                <style type="text/css">
                    input#category {
                        cursor: none;
                        pointer-events: none;
                    }

                </style>
                <div class="form-group col-md-4">
                    {{ Form::label('category', 'Category') }}
                    {{ Form::text('category',$value=null, ['class' => 'form-control','placeholder'=>'Select Rank first']) }}
                </div>
            </div>

            
            <!-- Status -->

            <div class="row">
                <div class="form-group col-md-4">
                    {{ Form::label('assigned', 'Assigned') }}
                    {{ Form::select('assigned',
                     [ 
                        'NR' => 'Not reported yet',
                        'LL' => 'LL', 
                        'VM' => 'VM',
                        'MD' => 'MD',
                        'MJ' => 'MJ',
                        'MC' => 'MP',
                        'MZ' => 'MZ'
                        
                     ],
                     null, 
                     ['class' => 'form-control']) }}
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        {{ Form::label('remarks', 'Remarks') }}
                        {{ Form::text('remarks', null, array('class' => 'form-control')) }}
                    </div>
                </div>
                
            </div>

            <div class="row">
                <div class="form-group col-md-4 stat">
                    {{ Form::label('status', 'Status') }}
                    {{ Form::select('status',
                     [ 
                        'Duty' => 'Duty', 
                        'Leave' => 'Leave',
                        'Hospitalized' => 'Hospitalized',
                        'Mission' => 'Mission',
                        'Schooling' => 'Schooling',
                        'Pass' => 'Pass',
                        'others' => 'Others'
                     ],
                     null, 
                     ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-md-4">
                    {{ Form::label('statusDate', 'Status started at') }}
                    <br>
                    {{ Form::date('statusDate', \Carbon\Carbon::now()) }}
                </div>
                <div class="form-group col-md-4">
                    {{ Form::label('daysGiven', 'How many days?') }}
                    <br>
                    {{ Form::number('daysGiven', null, array('class' => 'form-control')) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('image', 'Image') }}
                    {{ Form::file('image',array('class' => 'form-control')) }}
                    </div>
                </div>
            </div>
            <hr>
            <a href="{{url()->previous()}}" class="btn btn-default">Cancel</a>
             {{ Form::submit('Done', array('class' => 'btn btn-default')) }}
            {!! Form::close() !!}

        </div>

@endsection