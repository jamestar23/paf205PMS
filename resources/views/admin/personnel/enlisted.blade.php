@extends('admin.layout.admin')

@section('content')
<h3 class="text-center content-header">List of Enlisted Personnels</h3>
<hr>
<!-- personnels called from personnel controller -->

<table id="personnel-list" style="width:100%" class="text-left table table-striped table-bordered">
  <tr>
    <!-- <th>ID</th> -->
    <th>Rank</th>
    <!-- <th>Last Name</th> 
    <th>First Name</th>
    <th>Middle Name</th> -->
    <th>Name</th> 
    <th>AFP Serial No.</th> 
    <th>Assigned</th> 
    <th>Status</th>
    <th>Status started at</th>
    <th>Remarks</th>
    <th>Action</th>
    <!-- <th>Image</th> -->
  </tr>
  @forelse($enlisted as $ep) 
  <tr>
    <!-- <td>{{$ep->id}}</td> -->
    <td>{{$ep->rank}}</td>
    <!-- <td>{{$ep->lname}}</td> 
    <td>{{$ep->fname}}</td>
    <td>{{$ep->mname}}</td> -->
    <td>{{$ep->lname}}, {{$ep->fname}} {{$ep->mname}}</td>
    <td>{{$ep->afpsn}}</td> 
    <td>{{$ep->assigned}}</td>
    <td>{{$ep->status}}</td>
    <td>{{$ep->statusDate}}</td>
    <td>{{$ep->remarks}}</td> 
    <!-- <td><img src="{{url('images',$ep->image)}}" alt="2x2" width="96" height="96" /></td>  -->
    <td>
        <a href="{{route('personnel.edit',$ep->id)}}" >Edit</a>
        |
        <a href="{{action('PersonnelController@show', $ep['id'])}}">View</a>
    </td> 
  </tr>
  @empty
    <h3>No Personnel</h3>
  @endforelse
</table>

<hr>

<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home">Recap</a></li>
  <li><a data-toggle="tab" href="#duty">On Duty</a></li>
  <li><a data-toggle="tab" href="#leave">On Leave</a></li>
  <li><a data-toggle="tab" href="#hospitalized">Hospitalized</a></li>
  <li><a data-toggle="tab" href="#others">Others</a></li>
</ul>

<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    <table id="personnel-list" style="width:100%" class="text-left table table-striped table-bordered">
      <tr>
        <th><b>On Duty</b></th>
        <th><b>On Leave</b></th>
        <th><b>Hospitalized</b></th>
        <th><b>Others</b></th>
      </tr>
      <tr>
        <td>{{count($enlistedOD)}}</td>
        <td>{{count($enlistedOL)}}</td>
        <td>{{count($enlistedOH)}}</td>
        <td>{{count($enlistedO)}}</td>
      </tr>
    </table>
  </div>
  <div id="duty" class="tab-pane fade">
    <table id="personnel-list" style="width:100%" class="text-left table table-striped table-bordered">
      <tr>
        <th>Rank</th>
        <th>Last Name</th> 
        <th>First Name</th>
        <th>Middle Name</th>
        <th>AFP Serial No.</th> 
        <th>Assigned</th> 
        <th>Remarks</th>
        <!-- <th>Image</th> -->
      </tr>
      @forelse($enlistedOD as $enlisted) 
      <tr>
        <td>{{$enlisted->rank}}</td>
        <td>{{$enlisted->lname}}</td> 
        <td>{{$enlisted->fname}}</td>
        <td>{{$enlisted->mname}}</td>
        <td>{{$enlisted->afpsn}}</td> 
        <td>{{$enlisted->assigned}}</td>
        <td>{{$enlisted->remarks}}</td> 
      </tr>
      @empty
        <h3>No Personnel On Duty</h3>
      @endforelse
    </table>
  </div>
  <div id="leave" class="tab-pane fade">
    <table id="personnel-list" style="width:100%" class="text-left table table-striped table-bordered">
      <tr>
        <th>Rank</th>
        <th>Last Name</th> 
        <th>First Name</th>
        <th>Middle Name</th>
        <th>AFP Serial No.</th> 
        <th>Assigned</th> 
        <th>Remarks</th>
        <!-- <th>Image</th> -->
      </tr>
      @forelse($enlistedOL as $enlisted) 
      <tr>
        <td>{{$enlisted->rank}}</td>
        <td>{{$enlisted->lname}}</td> 
        <td>{{$enlisted->fname}}</td>
        <td>{{$enlisted->mname}}</td>
        <td>{{$enlisted->afpsn}}</td> 
        <td>{{$enlisted->assigned}}</td>
        <td>{{$enlisted->remarks}}</td> 
      </tr>
      @empty
        <h3>No Personnel On Leave</h3>
      @endforelse
    </table>
  </div>
  <div id="hospitalized" class="tab-pane fade">
    <table id="personnel-list" style="width:100%" class="text-left table table-striped table-bordered">
      <tr>
        <th>Rank</th>
        <th>Last Name</th> 
        <th>First Name</th>
        <th>Middle Name</th>
        <th>AFP Serial No.</th> 
        <th>Assigned</th> 
        <th>Remarks</th>
        <!-- <th>Image</th> -->
      </tr>
      @forelse($enlistedOH as $enlisted) 
      <tr>
        <td>{{$enlisted->rank}}</td>
        <td>{{$enlisted->lname}}</td> 
        <td>{{$enlisted->fname}}</td>
        <td>{{$enlisted->mname}}</td>
        <td>{{$enlisted->afpsn}}</td> 
        <td>{{$enlisted->assigned}}</td>
        <td>{{$enlisted->remarks}}</td> 
      </tr>
      @empty
        <h3>No Personnel Hospitalized</h3>
      @endforelse
    </table>
  </div>
  <div id="others" class="tab-pane fade">
    <table id="personnel-list" style="width:100%" class="text-left table table-striped table-bordered">
      <tr>
        <th>Rank</th>
        <th>Last Name</th> 
        <th>First Name</th>
        <th>Middle Name</th>
        <th>AFP Serial No.</th> 
        <th>Assigned</th> 
        <th>Remarks</th>
        <!-- <th>Image</th> -->
      </tr>
      @forelse($enlistedO as $enlisted) 
      <tr>
        <td>{{$enlisted->rank}}</td>
        <td>{{$enlisted->lname}}</td> 
        <td>{{$enlisted->fname}}</td>
        <td>{{$enlisted->mname}}</td>
        <td>{{$enlisted->afpsn}}</td> 
        <td>{{$enlisted->assigned}}</td>
        <td>{{$enlisted->remarks}}</td> 
      </tr>
      @empty
        <h3>No Personnel</h3>
      @endforelse
    </table>
  </div>
</div>
@endsection