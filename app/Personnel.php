<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personnel extends Model
{
    protected $fillable=['lname','fname','mname','afpsn','category','rank','status','statusDate','daysGiven','assigned','remarks','image'];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    // scoping category
    public function scopeOfficer($query)
    {
        return $query->where('category','Officer');
    }
    public function scopeEP($query)
    {
        return $query->where('category','Enlisted Personnel');
    }

    // scoping status
    public function scopeDuty($query)
    {
        return $query->where('status','duty');
    }
    public function scopeLeave($query)
    {
        return $query->where('status','leave');
    }
    public function scopeHospitalized($query)
    {
        return $query->where('status','hospitalized');
    }
    public function scopeOthers($query)
    {
        return $query->where('status','others');
    }

}
