<?php

namespace App\Http\Controllers;

use App\Personnel;
use Illuminate\Http\Request;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $personnels=personnel::all();
        $dtoday = Carbon::today()->toFormattedDateString();
        return view('user.home', compact('personnels'));
    }

    public function officers()
    {
        $officers=personnel::Officer()->get();

        $officerOD=personnel::Officer()->Duty()->get();
        $officerOL=personnel::Officer()->Leave()->get();
        $officerOH=personnel::Officer()->Hospitalized()->get();
        $officerO=personnel::Officer()->Others()->get();
        return view('admin.personnel.officers', compact('officers','officerOD','officerOL','officerOH','officerO'));
    }

    public function enlisted()
    {
        $enlisted=personnel::EP()->get();

        $enlistedOD=personnel::EP()->Duty()->get();
        $enlistedOL=personnel::EP()->Leave()->get();
        $enlistedOH=personnel::EP()->Hospitalized()->get();
        $enlistedO=personnel::EP()->Others()->get();
        return view('admin.personnel.enlisted', compact('enlisted','enlistedOD','enlistedOL','enlistedOH','enlistedO'));
    }
}
