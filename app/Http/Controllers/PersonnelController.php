<?php

namespace App\Http\Controllers;

use App\Personnel;
use Illuminate\Http\Request;
use Carbon\Carbon;

class PersonnelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personnels=personnel::all();

        //$personnels=personnel::where('category','Officer')->get();

        // $officerOD=personnel::Officer()->Duty()->get();
        return view('admin.personnel.index', compact('personnels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.personnel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formInput=$request->except('image');
        

        //validation
        $this->validate($request,[
            'lname'=>'required',
            'fname'=>'required',
            'mname'=>'required',
            'afpsn'=>'required',
            'category'=>'required',
            'rank'=>'required',
            'status'=>'required',
            'statusDate'=>'required',
            'assigned'=>'required',
            'remarks'=>'nullable',
            'image'=>'image|mimes:png,jpg,jpeg|max:10000'
        ]);

        // image upload
        $image=$request->image;
        if($image){
            $imageName=$image->getClientOriginalName();
            $image->move('images',$imageName);
            $formInput['image']=$imageName;
        }

        Personnel::create($formInput);
        //return redirect()->route('admin.index');
        // return back();
        return redirect()->route('personnel.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $personnel = Personnel::find($id);

        $sDate = Carbon::parse($personnel['statusDate']);
        $eDate = $sDate->addDays(7);

        return view('admin.personnel.personnel',compact('personnel','id','eDate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $personnel=Personnel::find($id);
        return view('admin.personnel.edit',compact(['personnel']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $personnel=Personnel::find($id);

        $formInput=$request->except('image');
//        validation
        $this->validate($request,[
            'lname'=>'required',
            'fname'=>'required',
            'mname'=>'required',
            'afpsn'=>'required',
            'category'=>'required',
            'rank'=>'required',
            'status'=>'required',
            'statusDate'=>'required',
            'assigned'=>'required',
            'remarks'=>'nullable',
            'image'=>'image|mimes:png,jpg,jpeg|max:10000'
        ]);
        //        image upload
        $image=$request->image;
        if($image){
            $imageName=$image->getClientOriginalName();
            $image->move('images',$imageName);
            $formInput['image']=$imageName;
        }
         $personnel->update($formInput);
        return redirect()->route('personnel.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Personnel::destroy($id);
        return redirect()->route('personnel.index');
    }

    // public function officersduty(){
    //     $personnels=personnel::where('rank', 'Officer');
    //     return view('admin.personnel.officersduty', compact('personnels'));
    // }

    public function officers()
    {
        $officers=personnel::Officer()->get();

        $officerOD=personnel::Officer()->Duty()->get();
        $officerOL=personnel::Officer()->Leave()->get();
        $officerOH=personnel::Officer()->Hospitalized()->get();
        $officerO=personnel::Officer()->Others()->get();
        return view('admin.personnel.officers', compact('officers','officerOD','officerOL','officerOH','officerO'));
    }

    public function enlisted()
    {
        $enlisted=personnel::EP()->get();

        $enlistedOD=personnel::EP()->Duty()->get();
        $enlistedOL=personnel::EP()->Leave()->get();
        $enlistedOH=personnel::EP()->Hospitalized()->get();
        $enlistedO=personnel::EP()->Others()->get();
        return view('admin.personnel.enlisted', compact('enlisted','enlistedOD','enlistedOL','enlistedOH','enlistedO'));
    }


    public function dashboard() {
        $dtoday = Carbon::today()->toFormattedDateString();
        $newPersonnel = personnel::orderBy('id', 'desc')->take(2)->get();
        return view('admin.index', compact('newPersonnel', 'dtoday'));
    }
}
